package edu.westga.cs1302.coursecheck.model;

import java.util.ArrayList;
import java.util.Collection;

/**
 * The Class Student.
 *
 * @author CS 1302
 */
public class Student {

	private String name;
	private Collection<CourseGrade> courseGrades;

	private static final String COURSES_CANNOT_BE_NULL = "courses cannot be null";
	private static final String COURSE_CANNOT_BE_NULL = "course cannot be null";
	private static final String NAME_CANNOT_BE_NULL = "name cannot be null";

	/**
	 * Instantiates a new student.
	 * 
	 * @precondition name != null
	 * @postcondition none
	 *
	 * @param name the name of the student
	 */
	public Student(String name) {
		if (name == null) {
			throw new IllegalArgumentException(Student.NAME_CANNOT_BE_NULL);
		}
		this.name = name;
		this.courseGrades = new ArrayList<CourseGrade>();
	}

	/**
	 * Gets the name.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Adds the course.
	 * 
	 * @precondition courseGrade != null
	 * @postcondition none
	 *
	 * @param courseGrade the course grade to be added
	 * 
	 * @return true if the course was added
	 */
	public boolean addCourseGrade(CourseGrade courseGrade) {
		if (courseGrade == null) {
			throw new IllegalArgumentException(Student.COURSE_CANNOT_BE_NULL);
		}
		return this.courseGrades.add(courseGrade);
	}

	@Override
	public String toString() {
		String str = this.getName() + " - Grades: ";
		for (CourseGrade grade : this.courseGrades) {
			str = str + grade + " ";
		}
		return str;
	}

	/**
	 * Adds the course.
	 * 
	 * @precondition courseName != null && 0 <= grade <= 4
	 * @postcondition none
	 *
	 * @param courseName the name of the course
	 * @param grade      the grade in the course
	 * @return true if the course was added
	 */
	public boolean addCourseGrade(String courseName, int grade) {
		CourseGrade courseGrade = new CourseGrade(courseName, grade);
		return this.courseGrades.add(courseGrade);
	}

	/**
	 * Checks if the specified course has been passed with the specified grade. The
	 * course may have been attempted several times with a lower grade.
	 *
	 * @precondition course != null
	 * @postcondition none
	 * 
	 * @param course         the course to be checked for
	 * @param minGradePoints the minimum grade points to pass the course
	 * @return true, if this student has passed the course
	 */
	public boolean passed(String course, int minGradePoints) {
		if (course == null) {
			throw new IllegalArgumentException(Student.COURSE_CANNOT_BE_NULL);
		}

		// TODO
		return false;
	}

	/**
	 * Checks if all attempts of the specified course have a grade point below the
	 * specified grade points.
	 *
	 * @precondition course != null
	 * @postcondition none
	 * 
	 * @param course         the course to be checked for
	 * @param minGradePoints the minimum grade points to pass the course
	 * @return true, if this student has attempted the course and all attempts have
	 *         failed
	 */
	public boolean failed(String course, int minGradePoints) {
		if (course == null) {
			throw new IllegalArgumentException(Student.COURSE_CANNOT_BE_NULL);
		}

		// TODO
		return false;
	}

	/**
	 * Checks if this student took all of the specified courses, regardless of the
	 * grade point in the course. Note: The list of specified courses may contain
	 * duplicates.
	 *
	 * @precondition courses != null
	 * @postcondition none
	 * 
	 * @param courses the courses to be checked
	 * @return true, if this student has taken all specified courses
	 */
	public boolean tookAllOf(ArrayList<String> courses) {
		if (courses == null) {
			throw new IllegalArgumentException(Student.COURSES_CANNOT_BE_NULL);
		}

		// TODO
		return false;
	}

	/**
	 * Checks if this student took none of the specified courses. Note: The list of
	 * specified courses may contain duplicates.
	 *
	 * @precondition courses != null
	 * @postcondition none
	 * 
	 * @param courses the courses to be checked
	 * @return true, if this student has taken none of the specified courses
	 */
	public boolean tookNoneOf(ArrayList<String> courses) {
		if (courses == null) {
			throw new IllegalArgumentException(Student.COURSES_CANNOT_BE_NULL);
		}

		// TODO
		return false;
	}

	/**
	 * Checks if this student took at least one of the specified courses. Note: The
	 * list of specified courses may contain duplicates.
	 * 
	 * @precondition courses != null
	 * @postcondition none
	 * 
	 * @param courses the courses to be checked
	 * @return true, if this student has taken at least one of the specified courses
	 */
	public boolean tookAtLeastOneOf(ArrayList<String> courses) {
		if (courses == null) {
			throw new IllegalArgumentException(Student.COURSES_CANNOT_BE_NULL);
		}

		// TODO
		return false;
	}

	/**
	 * Checks if this student took at most one of the specified courses. Note: The
	 * list of specified courses may contain duplicates. If this student took a
	 * course that is listed more than once in list courses and did not take any
	 * other course in the list, the method returns true.
	 * 
	 * @precondition courses != null
	 * @postcondition none
	 * 
	 * @param courses the courses to be checked
	 * @return true, if this student has taken at most one of the specified courses
	 */
	public boolean tookAtMostOneOf(ArrayList<String> courses) {
		if (courses == null) {
			throw new IllegalArgumentException(Student.COURSES_CANNOT_BE_NULL);
		}

		// TODO
		return false;
	}

	/**
	 * Checks to see if this students has taken and passed all courses that are
	 * listed in the specified list. A student has passed a course if the student
	 * has received a grade point of at least the minGradePoints in at least one
	 * attempt of that course.
	 * 
	 * @precondition courses != null
	 * @postcondition none
	 * 
	 * @param courses        the courses to be checked
	 * @param minGradePoints the minimum grade points to pass the course
	 * @return true, if this student has taken and passed all the specified courses
	 *         with specified grade or better
	 */
	public boolean hasTakenAndPassed(ArrayList<String> courses, int minGradePoints) {
		if (courses == null) {
			throw new IllegalArgumentException(Student.COURSES_CANNOT_BE_NULL);
		}

		// TODO
		return false;
	}
}
