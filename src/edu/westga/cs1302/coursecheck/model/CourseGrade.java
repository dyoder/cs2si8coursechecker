/**
 * 
 */
package edu.westga.cs1302.coursecheck.model;

/**
 * The class Course.
 * 
 * @author CS1302
 */
public class CourseGrade {

	private String course;
	private int gradePoints;
	private static final String INVALID_GRADE = "invalid grade points";

	/**
	 * Instantiates a new course grade.
	 *
	 * @precondition course != null && 0 <= gradePoints <=4
	 * @postcondition getCourse() = course && getGradePoints() == gradePoints
	 * 
	 * @param course
	 *            the course name
	 * @param gradePoints
	 *            grade points in the specified course
	 */
	public CourseGrade(String course, int gradePoints) {
		if (course == null) {
			throw new IllegalArgumentException("course name cannot be null");
		}
		if (gradePoints < 0 || gradePoints > 4) {
			throw new IllegalArgumentException(CourseGrade.INVALID_GRADE);
		}
		this.course = course;
		this.gradePoints = gradePoints;
	}

	/**
	 * Gets the grade points.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the grade points
	 */
	public int getGradePoints() {
		return this.gradePoints;
	}

	/**
	 * Sets the grade points.
	 * 
	 * @precondition 0 <= gradePoints <=4
	 * @postcondition getGradePoints() == gradePoints
	 *
	 * @param gradePoints
	 *            the new grade points
	 */
	public void setGradePoints(int gradePoints) {
		if (gradePoints < 0 || gradePoints > 4) {
			throw new IllegalArgumentException(CourseGrade.INVALID_GRADE);
		}
		this.gradePoints = gradePoints;
	}

	/**
	 * Gets the course name.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the course name
	 */
	public String getCourse() {
		return this.course;
	}

	@Override
	public String toString() {
		return this.getCourse() + ":" + this.getGradePoints();
	}
}
