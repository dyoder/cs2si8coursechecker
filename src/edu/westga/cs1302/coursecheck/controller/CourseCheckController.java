package edu.westga.cs1302.coursecheck.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

import edu.westga.cs1302.coursecheck.model.Student;

/**
 * The Class CourseChecker.
 * 
 * @author CS 1302
 */
public class CourseCheckController {

	private Collection<Student> students;

	/**
	 * Instantiates a CourseChecker.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public CourseCheckController() {
		this.students = new ArrayList<Student>();
	}

	/**
	 * Instantiates a CourseChecker.
	 * 
	 * @precondition studentfile is a valid text file with student input
	 * @postcondition students contains all students specified in studentfile
	 * 
	 * @param studentfile
	 *            file with students and courses
	 */
	public CourseCheckController(String studentfile) {
		this.students = new ArrayList<Student>();
		this.readStudentsFromFile(studentfile);
	}

	/**
	 * Runs several checks on list of all students.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void runChecks() {
		this.printStudentsPassingCS1();
		this.printStudentsFailingCS1();
		this.printStudentsTakenAllIntroCS();
		this.printStudentsTakenNoIntroCS();
		this.printStudentsTakenAtLeastOneIntroCS();
		this.printStudentsTakenAtMostOneIntroCS();
		this.printStudentsTakenAndPassedCourses();
	}

	/**
	 * Read the student data from file.
	 *
	 * @precondition studentfile is a file with valid student data
	 * @postcondition none
	 * 
	 * @param studentfile
	 *            the name of the file with the student data
	 */
	public void readStudentsFromFile(String studentfile) {
		File file = new File(studentfile);
		try (Scanner input = new Scanner(file)) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] tokens = line.split(",");
				Student student = new Student(tokens[0]);
				for (int i = 1; i < tokens.length; i++) {
					String[] course = (tokens[i]).split(":");
					int grade = Integer.parseInt(course[1]);
					student.addCourseGrade(course[0], grade);
				}
				this.students.add(student);
			}
		} catch (Exception e) {
			System.err.println("Could not read file: " + e.getMessage());
		}
	}

	/**
	 * Prints all students that passed CS1 with grade point >= 3
	 */
	private void printStudentsPassingCS1() {
		System.out.println(System.lineSeparator() + "***  Students who passed CS1 with >= 3 ***");
		for (Student curStudent : this.students) {
			if (curStudent.passed("CS1301", 3)) {
				System.out.println(curStudent);
			}
		}
	}

	/**
	 * Prints all students that failed CS1 with grade point < 3
	 */
	private void printStudentsFailingCS1() {
		System.out.println(System.lineSeparator() + "***  Students who failed CS1 with < 3 ***");
		for (Student curStudent : this.students) {
			if (curStudent.failed("CS1301", 3)) {
				System.out.println(curStudent);
			}
		}
	}

	/**
	 * Prints all students that took all introductory CS classes
	 */
	private void printStudentsTakenAllIntroCS() {
		System.out.println(System.lineSeparator() + "***  Students who took CS1300, CS1301, and CS1302 ***");
		ArrayList<String> courses = new ArrayList<String>(Arrays.asList("CS1300", "CS1301", "CS1302"));
		for (Student curStudent : this.students) {
			if (curStudent.tookAllOf(courses)) {
				System.out.println(curStudent);
			}
		}
	}

	/**
	 * Prints all students that took no introductory CS classes
	 */
	private void printStudentsTakenNoIntroCS() {
		System.out.println(System.lineSeparator() + "***  Students who took none of CS1300, CS1301, and CS1302 ***");
		ArrayList<String> courses = new ArrayList<String>(Arrays.asList("CS1300", "CS1301", "CS1302"));
		for (Student curStudent : this.students) {
			if (curStudent.tookNoneOf(courses)) {
				System.out.println(curStudent);
			}
		}
	}

	/**
	 * Prints all students that took at least one introductory CS class
	 */
	private void printStudentsTakenAtLeastOneIntroCS() {
		System.out.println(
				System.lineSeparator() + "***  Students who took at least one of CS1300, CS1301, and CS1302 ***");
		ArrayList<String> courses = new ArrayList<String>(Arrays.asList("CS1300", "CS1301", "CS1302"));
		for (Student curStudent : this.students) {
			if (curStudent.tookAtLeastOneOf(courses)) {
				System.out.println(curStudent);
			}
		}
	}

	/**
	 * Prints all students that took at most one introductory CS class
	 */
	private void printStudentsTakenAtMostOneIntroCS() {
		System.out.println(
				System.lineSeparator() + "***  Students who took at most one of CS1300, CS1301, and CS1302 ***");
		ArrayList<String> courses = new ArrayList<String>(Arrays.asList("CS1300", "CS1301", "CS1302"));
		for (Student curStudent : this.students) {
			if (curStudent.tookAtMostOneOf(courses)) {
				System.out.println(curStudent);
			}
		}
	}

	/**
	 * Prints all students that are passing CS1301, CS1302, CS3151, CS3152 in
	 * sequence
	 */
	private void printStudentsTakenAndPassedCourses() {
		System.out.println(System.lineSeparator()
				+ "***  Students who have taken and have passed CS1301, CS1302 with a B or better ***");
		ArrayList<String> courses = new ArrayList<String>(Arrays.asList("CS1301", "CS1302"));
		for (Student curStudent : this.students) {
			if (curStudent.hasTakenAndPassed(courses, 3)) {
				System.out.println(curStudent);
			}
		}
	}
}
