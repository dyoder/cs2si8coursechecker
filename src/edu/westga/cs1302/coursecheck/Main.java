package edu.westga.cs1302.coursecheck;

import edu.westga.cs1302.coursecheck.controller.CourseCheckController;

/**
 * Entry point for the program.
 * 
 * @author CS 1302
 */
public class Main {

	/**
	 * Default entry point for the program.
	 * 
	 * @param args
	 *            command line arguments for the program
	 */
	public static void main(String[] args) {
		CourseCheckController checker = new CourseCheckController("students.txt");
		checker.runChecks();
	}

}
